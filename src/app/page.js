import Header from "./components/Header";
import Hero from "./components/Hero";
import About from "./components/About";
import Footer from "./components/Footer";
import BackTopBtn from "./components/BackTopBtn";
import Telechargement from "./components/Telechargement";
import Informations from "./components/Informations";
import Contact from "./components/Contact";
import Testimonial from "./components/Testimonial";


export default function Home() {
  return (
    <main className="max-w-[1920px] bg-white mx-auto 
    relative overflow-hidden">
      <Header />
      <Hero />
      <About />
      <Telechargement />
      <Informations />
      {/* <Testimonial/> */}
      {/* <Contact /> */}
      <Footer />
      {/* <div className='h-[4000px]'></div> */}
    </main>
  );
}
