'use client'



import Image from 'next/image'

// 
import { motion, easeInOut } from 'framer-motion';

// 
import { fadeIn } from '../../../variantts';

function Telechargement() {
    return (
        <section className="section flex items-center "
            id="telechargement">
            <div className=" container mx-auto h-full" >
                {/* text & img wrapper */}
                <div className="flex flex-col xl:flex-row justify-center items-center
                xl:justify-start h-full" >
                    {/*  */}
                    <motion.div
                        variants={fadeIn('up', 0.6)}
                        initial="hidden"
                        whileInView={"show"}
                        viewport={{ once: false, amount: 0.6 }}
                        className=" relative w-full h-full max-h-[50vh] md:max-w-[70vw] 
                        xl:max-w-[860px] xl:max-h-[542px] xl:-left-[100px] min-[1680px] 
                        :left-[120px]" >
                        <Image
                            src={'/assets/images/2.svg'}
                            fill
                            alt=""
                            style={{ objectFit: 'contain' }}

                        />
                    </motion.div>
                    {/* text */}
                    <div className="text-center xl:max-w-xl xl:text-left mt-16 xl:mt-0">
                        <div className="" >
                            <motion.h1
                                variants={fadeIn('down', 0.2)}
                                initial="hidden"
                                whileInView={"show"}
                                viewport={{ once: false, amount: 0.6 }}
                                className="h1"
                            >
                                Obtenir l’Appication.
                            </motion.h1>
                        </div>
                        <motion.p
                            variants={fadeIn('down', 0.4)}
                            initial="hidden"
                            whileInView={"show"}
                            viewport={{ once: false, amount: 0.6 }}
                            className="description max-w-[500px] mx-auto xl:mx-0 mb-6 xl:mb-10 " >
                            Notre application est disponible sur Play Store, App Store,
                            vous pouvez immédiatement la télécharger gratuitement et
                            profiter des fonctionnalités de l'application que nous
                            avons fournies
                        </motion.p>
                        <motion.div
                            variants={fadeIn('down', 0.6)}
                            initial="hidden"
                            whileInView={"show"}
                            viewport={{ once: false, amount: 0.8 }}
                            className="flex gap-x-3 justify-center xl:justify-start">
                            {/* btn appstore */}
                            <button className="btn-cat">
                                <Image
                                    src={'/assets/icons/buttons/app-store.svg'}
                                    width={132}
                                    height={36}
                                    alt="" />
                            </button>
                            {/* btn google */}
                            <button className="btn-cat">
                                <Image
                                    src={'/assets/icons/buttons/google-play.svg'}
                                    width={132}
                                    height={36}
                                    alt="" />
                            </button>
                        </motion.div>
                    </div>
                    {/* img */}

                </div>
            </div>
        </section>

    )
}

export default Telechargement
