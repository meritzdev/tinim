"use client"

import React from 'react'
import Brands from './Brands'
import Slider from './Slider'

function About() {
    return (
        <section className="section flex items-center"
            id="about">
            <div className="container mx-auto">
                <Brands />
                <Slider />
            </div>
        </section>
    )
}

export default About
