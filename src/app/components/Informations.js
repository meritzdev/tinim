'use client'



import Image from 'next/image'

// 
import { motion, easeInOut } from 'framer-motion';

// 
import { fadeIn } from '../../../variantts';

function Informations() {
    return (
        <section className="section flex items-center "
            id="informations">
            <div className=" container mx-auto h-full xl:pt-10" >
                {/* text & img wrapper */}
                <div className="flex flex-col xl:flex-row justify-center items-center
                xl:justify-start h-full" >
                    {/* text */}
                    <motion.div
                        variants={fadeIn('down', 0.2)}
                        initial="hidden"
                        whileInView={"show"}
                        viewport={{ once: false, amount: 0.6 }}
                        className="text-center xl:max-w-xl xl:text-left mt-16 xl:mt-0">
                        <div className="" >
                            <h1 className="h1">
                                Tinim information
                            </h1>
                        </div>
                        <div className="flex flex-col  justify-center items-center">
                            <div className=" flex flex-col xl:flex-row gap-x-8">
                                <div className="flex flex-col">
                                    <Image src={'/assets/icons/1.svg'} width={40} height={40} alt="" />
                                </div>
                                <div className="flex flex-col">
                                    <div>
                                        <h2 className="h3">Informations opportunes</h2>
                                    </div>
                                    <div>
                                        <p
                                            className="" >
                                            Les informations que vous obtenez sont toujours mises à jour par nous, car
                                            Nous priorisons une information précise et rapide
                                        </p>

                                    </div>
                                </div>
                            </div>
                            <div className=" flex flex-col xl:flex-row gap-x-8">
                                <div className="flex flex-col">
                                    <Image src={'/assets/icons/2.svg'} width={40} height={40} alt="" />
                                </div>
                                <div className="flex flex-col">

                                    <h2 className="h3">Informations opportunes</h2>

                                    <p >Les informations que vous obtenez sont toujours mises à jour par nous, car
                                        Nous priorisons une information précise et rapide</p>

                                </div>
                            </div>
                            <div className=" flex flex-col xl:flex-row gap-x-8">
                                <div className="flex flex-col">
                                    <Image src={'/assets/icons/3.svg'} width={40} height={40} alt="" />
                                </div>
                                <div className="flex flex-col">

                                    <h2 className="h3">Informations opportunes</h2>


                                    <p>Les informations que vous obtenez sont toujours mises à jour par nous, car
                                        Nous priorisons une information précise et rapide
                                    </p>

                                </div>
                            </div>
                            <div className=" flex flex-col xl:flex-row gap-x-8">

                                <div>
                                    <button className="bg-preincipal h-[54px] rounded-[5px] px-4 transition-all duration-300 items-center">
                                        <h3 className="text-white font-light items-center">Voir la démo</h3>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </motion.div>
                    {/* img */}
                    <motion.div
                        variants={fadeIn('up', 0.6)}
                        initial="hidden"
                        whileInView={"show"}
                        viewport={{ once: false, amount: 0.6 }}
                        className="relative w-full h-full max-h-[50vh] md:max-w-[70vw]
                    xl:max-w-[860px] xl:max-h-[542px]  xl:-right-[100px] min-[1680px] 
                    :right-[120px]  " >
                        <Image
                            src={'/assets/images/3.svg'}
                            fill
                            alt=""
                            style={{ objectFit: 'contain' }}
                            priority
                        />
                    </motion.div>
                </div>
            </div>
        </section>

    )
}

export default Informations
