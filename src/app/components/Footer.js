"use client "

// next image
import Image from "next/image";



//icons
import { FaPhone, FaEnvelope } from "react-icons/fa6";

//components
import Copyright from "./Copyright";

import { motion, easeInOut } from 'framer-motion';

// 
import { fadeIn } from '../../../variantts';



export default function Footer() {
    return (
        <footer className="pt-20 z-20 bg-preincipal" >
            <div
                className="container mx-auto">
                {/* grid */}
                <div className="flex flex-col lg:flex-row lg:justify-between">
                    <div className="flex flex-col flex-1 gap-y-8">
                        {/* logo */}
                        <Image src={'/assets/images/logo2.svg'} width={80} height={80} alt="" />

                        <div className="text-white h3 font-light">
                            Nous sommes une application de service
                            la société en matière de transaction
                            surtout en sécurité informatique
                        </div>
                        <div className="flex flex-col gap-y-4 font-semibold">
                            <div className="flex items-center gap-x-[10px]">
                                < FaPhone />
                                <div className="text-white font-medium">
                                    (+228)90-00-80-20 / 99-99-49-99
                                </div>
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <FaEnvelope />
                                <div className="text-white font-medium">
                                    modestemode@live.fr
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="flex-1 flex flex-col xl:items-center">
                        <div>
                            <h3 className="text-white h3 font-bold mb-8">Entreprise</h3>
                            <ul className="flex flex-col gap-y-4 font-semibold ">
                                <li>
                                    <a href="" className="text-white font-medium">coes group</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="flex-1 flex flex-col xl:items-center">
                        <div>
                            <h3 className="text-white h3 font-bold mb-8">Soutien</h3>
                            <ul className="flex flex-col gap-y-4 font-semibold ">
                                <li>
                                    <a href="" className="text-white font-medium">COES GROUP</a>
                                </li>
                                <li>
                                    <a href="" className="text-white font-medium">M²TECNOLOGIE</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="flex-1 flex flex-col xl:items-center">
                        <div>
                            <h3 className="text-white h3 font-bold mb-8">Menu</h3>
                            <ul className="flex flex-col gap-y-4 font-semibold ">
                                <li>
                                    <a href="" className="text-white font-medium">Home</a>
                                </li>
                                <li>
                                    <a href="" className="text-white font-medium">À propos</a>
                                </li>
                                <li>
                                    <a href="" className="text-white font-medium">Telechargement</a>
                                </li>
                                <li>
                                    <a href="" className="text-white font-medium">Informations</a>
                                </li>
                                <li>
                                    <a href="" className="text-white font-medium">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}
