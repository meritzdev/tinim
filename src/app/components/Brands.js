'use client'



import Image from 'next/image'
// 
import { motion, easeInOut } from 'framer-motion';

// 
import { fadeIn } from '../../../variantts';

export default function Brands() {
    return (
        <div className="text-center xl:bottom-48">
            <div className="" >
                <motion.h1
                    variants={fadeIn('down', 0.2)}
                    initial="hidden"
                    whileInView={"show"}
                    viewport={{ once: false, amount: 0.6 }}
                    className="h1"
                >
                    Qu’est-ce que le <spam>Tinim</spam>
                </motion.h1>
            </div>
            <motion.p
                variants={fadeIn('down', 0.4)}
                initial="hidden"
                whileInView={"show"}
                viewport={{ once: false, amount: 0.6 }}
                className="description max-w-[500px] mx-auto  " >
                Application pour aider les citoyens ici,
                vous pouvez envoyer de l'argent et suivre
                la situation des étape de votre transaction
                entre les utilisateurs
                de Tinim, des actualités, des données
            </motion.p>
        </div>
    )
}
