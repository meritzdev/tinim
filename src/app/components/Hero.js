'use client'

import { useContext } from 'react'
// components
import Search from './Search'
import { SearchContext } from '../context/search'

import Image from 'next/image'

// 
import { motion, easeInOut } from 'framer-motion';

// 
import { fadeIn } from '../../../variantts';

export default function Hero() {
    const { searchActive } = useContext(SearchContext)
    return (
        <section className="h-screen xl:h-[90vh]" id='home'>
            <div className=" container mx-auto h-full xl:pt-10" >
                {/* text & img wrapper */}
                <div className="flex flex-col xl:flex-row justify-center items-center
                xl:justify-start h-full" >
                    {/* text */}
                    <div className="text-center xl:max-w-xl xl:text-left mt-16 xl:mt-0">
                        <div className="" >
                            <motion.h1
                                variants={fadeIn('down', 0.2)}
                                initial="hidden"
                                whileInView={"show"}
                                viewport={{ once: false, amount: 0.6 }}
                                className="h1"
                            >
                                Envoie plus, dépense moins.
                            </motion.h1>
                        </div>
                        <motion.p
                            variants={fadeIn('down', 0.4)}
                            initial="hidden"
                            whileInView={"show"}
                            viewport={{ once: false, amount: 0.6 }}
                            className="description max-w-[500px] mx-auto xl:mx-0 mb-6 xl:mb-10 " >
                            Envoyez de l'argent depuis le Royaume-Uni, l'UE, les États-Unis,
                            le Canada et les Émirats arabes unis vers l'Afrique, l'Asie,
                            les Caraïbes et l'Amérique latine à un tarif avantageux.
                            Grâce aux transferts rapides vers les portefeuilles d'argent
                            mobile sur les réseaux populaires, vous pouvez envoyer
                            de l'argent depuis votre smartphone à tout moment.
                        </motion.p>
                        <motion.div
                            variants={fadeIn('down', 0.6)}
                            initial="hidden"
                            whileInView={"show"}
                            viewport={{ once: false, amount: 0.8 }}
                            className="flex gap-x-3 justify-center xl:justify-start">
                            {/* btn appstore */}
                            <button className="btn-cat">
                                <Image
                                    src={'/assets/icons/buttons/app-store.svg'}
                                    width={132}
                                    height={36}
                                    alt="" />
                            </button>
                            {/* btn google */}
                            <button className="btn-cat">
                                <Image
                                    src={'/assets/icons/buttons/google-play.svg'}
                                    width={132}
                                    height={36}
                                    alt="" />
                            </button>
                        </motion.div>
                    </div>
                    {/* img */}
                    <motion.div
                        variants={fadeIn('up', 0.6)}
                        initial="hidden"
                        whileInView={"show"}
                        viewport={{ once: false, amount: 0.6 }}
                        className="relative w-full h-full max-h-[50vh] md:max-w-[70vw]
                    xl:max-w-[860px] xl:max-h-[542px] xl:absolute xl:-right-[100px] min-[1680px] 
                    :right-[120px] xl:top-48 " >
                        <Image
                            src={'/assets/images/1.svg'}
                            fill
                            alt=""
                            style={{ objectFit: 'contain' }}
                            priority
                        />
                    </motion.div>
                </div>
            </div>


            {/* {searchActive ? (
                <div className="fixed top-[80px] z-10 w-full max-w-[1920px]">
                    <Search />
                </div>
            ) : (
                <div className="-mt-12 w-full max-w-[1300px] mx-auto">
                    <Search />
                </div>
            )} */}
        </section>

    )
}
