'use client'



import Image from 'next/image'

// 
import { motion, easeInOut } from 'framer-motion';

// 
import { fadeIn } from '../../../variantts';

function Contact() {
    return (
        // <section className="section flex justify-center items-center h-screen "
        //     id="contact">
        //     <div className="flex flex-col xl:flex-row justify-between max-w-lg mx-auto bg-white rounded-lg shadow-lg p-6
        //     xl:justify-start h-full">
        //         <div>
        //             <h2 className="text-xl font-bold mb-4">Contactez-nous</h2>
        //             <div className="mb-4">
        //                 <label htmlFor="name" className="block text-sm font-semibold text-gray-700 mb-1">Votre Nom</label>
        //                 <input id="name" type="text" className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500" placeholder="Entrez votre nom" />
        //             </div>
        //             <div className="mb-4">
        //                 <label htmlFor="email" className="block text-sm font-semibold text-gray-700 mb-1">Adresse Email</label>
        //                 <input id="email" type="email" className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500" placeholder="Entrez votre adresse email" />
        //             </div>
        //             <div className="mb-6">
        //                 <label htmlFor="message" className="block text-sm font-semibold text-gray-700 mb-1">Votre Message</label>
        //                 <textarea id="message" rows="4" className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500" placeholder="Entrez votre message"></textarea>
        //             </div>
        //             <button className="bg-blue-500 hover:bg-blue-600 text-white font-semibold py-2 px-4 rounded focus:outline-none focus:ring focus:ring-blue-200">
        //                 Envoyer
        //             </button>
        //         </div>
        //         <div className="flex flex-col justify-between">
        //             <div>
        //                 <h3 className="text-lg font-semibold mb-2">Coordonnées</h3>
        //                 <p className="text-gray-700 mb-1">123 Rue de l'Exemple</p>
        //                 <p className="text-gray-700 mb-1">Ville, Pays</p>
        //                 <p className="text-gray-700 mb-1">Téléphone: +123 456 789</p>
        //                 <p className="text-gray-700">Email: exemple@example.com</p>
        //             </div>
        //             <div className="mt-6 text-sm text-gray-600">
        //                 <p>Horaires de travail :</p>
        //                 <p>Lundi - Vendredi : 9h00 - 17h00</p>
        //                 <p>Samedi - Dimanche : Fermé</p>
        //             </div>
        //         </div>
        //     </div>
        // </section>
        <section className="section flex items-center "
            id="contact">
            <div className=" container mx-auto " >
                {/* text & img wrapper */}
                <div className=" rounded-lg shadow-lg">
                    <div className="text-center xl:bottom-48" >
                        <motion.h1
                            variants={fadeIn('down', 0.2)}
                            initial="hidden"
                            whileInView={"show"}
                            viewport={{ once: false, amount: 0.6 }}
                            className="h1"
                        >
                            Contact
                        </motion.h1>
                    </div>
                    <div className="flex flex-col xl:flex-row xl:justify-between rounded-lg shadow-lg" >
                        {/*  */}
                        <motion.div
                            variants={fadeIn('up', 0.6)}
                            initial="hidden"
                            whileInView={"show"}
                            viewport={{ once: false, amount: 0.6 }}
                            className=" " >
                            <div className="flex flex-col  justify-center items-center">
                                <h2 className="text-xl font-bold mb-4">Laissez-nous un message</h2>
                                <div className="mb-4">
                                    <label htmlFor="name" className="block text-sm font-semibold text-gray-700 mb-1">Votre Nom</label>
                                    <input id="name" type="text" className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500" placeholder="Entrez votre nom" />
                                </div>
                                <div className="mb-4">
                                    <label htmlFor="email" className="block text-sm font-semibold text-gray-700 mb-1">Adresse Email</label>
                                    <input id="email" type="email" className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500" placeholder="Entrez votre adresse email" />
                                </div>
                                <div className="mb-6">
                                    <label htmlFor="message" className="block text-sm font-semibold text-gray-700 mb-1">Votre Message</label>
                                    <textarea id="message" rows="4" className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500" placeholder="Entrez votre message"></textarea>
                                </div>
                                <button className="bg-blue-500 hover:bg-blue-600 text-white font-semibold py-2 px-4 rounded focus:outline-none focus:ring focus:ring-blue-200">
                                    Envoyer
                                </button>
                            </div>
                        </motion.div>
                        {/* text */}
                        <div className="text-center xl:max-w-xl xl:text-left mt-16 xl:mt-0">
                            <motion.p
                                variants={fadeIn('down', 0.4)}
                                initial="hidden"
                                whileInView={"show"}
                                viewport={{ once: false, amount: 0.6 }}
                                className="description max-w-[500px] mx-auto xl:mx-0 mb-6 xl:mb-10 " >
                                <div className="flex flex-col justify-between">
                                    <div>
                                        <h3 className="text-lg font-semibold mb-2">Coordonnées</h3>
                                        <p className="text-gray-700 mb-1">123 Rue de l'Exemple</p>
                                        <p className="text-gray-700 mb-1">Ville, Pays</p>
                                        <p className="text-gray-700 mb-1">Téléphone: +123 456 789</p>
                                        <p className="text-gray-700">Email: exemple@example.com</p>
                                    </div>
                                    <div className="mt-6 text-sm text-gray-600">
                                        <p>Horaires de travail :</p>
                                        <p>Lundi - Vendredi : 9h00 - 17h00</p>
                                        <p>Samedi - Dimanche : Fermé</p>
                                    </div>
                                </div>
                            </motion.p>
                        </div>
                        {/* img */}

                    </div>
                </div>

            </div>
        </section>

    )
}

export default Contact
