"use client"

// 
import { Swiper, SwiperSlide } from 'swiper/react'

// 
import 'swiper/css'

// 
import Image from 'next/image'

// 
import { FaStar, FaStarHalfAll, FaRegStar } from 'react-icons/fa'

// 
import { motion, easeInOut } from 'framer-motion';

// 
import { fadeIn } from '../../../variantts';

const items = [
    {
        image: "",
        title: "Téléchargement de l'appli",
        description: "Cliquer sur le lien qui se trouve en haut de la page pour télécharger l'application.",
    },
    {
        image: "",
        title: "Installation et créaton de compte",
        description: "Renseignez les informations demandées (Nom complet et numéro de téléphone et code pin) pour a création de votre compte",
    },
    {
        image: "",
        title: "Faites votre premier transfert",
        description: "Sélectionner le type de transfert que vous voulez faire, le montant, le destinaire, votre code et le tour est joué!",
    }
]

function Slider() {
    return (
        <div className="container mx-auto xl:top-48" >
            <Swiper
                breakpoints={{
                    320: { slidesPerView: 1, spaceBetween: 15 },
                    640: { slidesPerView: 2, spaceBetween: 32 },
                    1260: { slidesPerView: 3, spaceBetween: 32 },
                }}
            >
                {items.map((item, index) => {
                    return (
                        <SwiperSlide key={index}>
                            <div className="max-w-sm mx-auto sm:mx-0 rounded shadow-lg ">
                                <div className="px-6 py-4">
                                    <div className="font-bold text-xl mb-2">{item.title}</div>
                                    <p className="text-gray-700 text-base">
                                        {item.description}
                                    </p>
                                </div>
                            </div>
                        </SwiperSlide>
                    )
                })}
            </Swiper>
        </div>
    )
}

export default Slider
